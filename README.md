# Jedi(绝地)
jedi is a dynamic thread pool executor

note: base on https://github.com/ctripcorp/apollo

# Screenshots

动态配置线程池参数

![动态配置线程池参数](https://images.gitee.com/uploads/images/2021/0905/170010_6be8758e_5057838.png)

线程池概览

![线程池概览](https://images.gitee.com/uploads/images/2021/0905/170437_924750df_5057838.png)

线程池实时状态监控

![线程池实时状态监控](https://images.gitee.com/uploads/images/2021/0905/170551_292e2bc5_5057838.png)

线程池任务监控

![线程池任务汇总监控](https://images.gitee.com/uploads/images/2021/0905/170714_e8371566_5057838.png)

![线程池任务明细监控](https://images.gitee.com/uploads/images/2021/0905/170748_df7d6765_5057838.png)

线程池监控报警对接公司IM系统

![线程池监控报警对接公司IM系统](https://images.gitee.com/uploads/images/2021/0812/075117_1327fb49_5057838.jpeg)

# Features

- ## 实时展示线程池状态

- ## 线程池指标异常报警

- ## 线程池参数动态调整

- ## 线程池及任务数据分析

# Usage

- 演示地址

  http://jedi.hellothomas.xyz:8080
  username: admin
  password: 123456

  创建应用→创建线程池→发布

- Java客户端使用指南

  https://github.com/hellothomas-group/jedi/wiki#%E4%B8%80-%E5%AE%A2%E6%88%B7%E7%AB%AF%E4%BD%BF%E7%94%A8%E6%8C%87%E5%8D%97

# Design

https://github.com/hellothomas-group/jedi/wiki#%E4%BA%8C-%E7%B3%BB%E7%BB%9F%E6%9E%B6%E6%9E%84

# Development

# Deployment

https://github.com/hellothomas-group/jedi/wiki#%E4%B8%89-%E6%9C%8D%E5%8A%A1%E7%AB%AF%E9%83%A8%E7%BD%B2%E6%8C%87%E5%8D%97

# Release Notes

# FAQ

# Contribution

# License

The project is licensed under the [Apache 2 license](https://github.com/hellothomas-group/jedi/blob/main/LICENSE).

# Known Users