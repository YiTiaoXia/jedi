package xyz.hellothomas.jedi.biz.domain.monitor;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class AppExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public AppExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Long value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Long value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Long value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Long value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Long value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Long value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Long> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Long> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Long value1, Long value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Long value1, Long value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andNamespaceNameIsNull() {
            addCriterion("namespace_name is null");
            return (Criteria) this;
        }

        public Criteria andNamespaceNameIsNotNull() {
            addCriterion("namespace_name is not null");
            return (Criteria) this;
        }

        public Criteria andNamespaceNameEqualTo(String value) {
            addCriterion("namespace_name =", value, "namespaceName");
            return (Criteria) this;
        }

        public Criteria andNamespaceNameNotEqualTo(String value) {
            addCriterion("namespace_name <>", value, "namespaceName");
            return (Criteria) this;
        }

        public Criteria andNamespaceNameGreaterThan(String value) {
            addCriterion("namespace_name >", value, "namespaceName");
            return (Criteria) this;
        }

        public Criteria andNamespaceNameGreaterThanOrEqualTo(String value) {
            addCriterion("namespace_name >=", value, "namespaceName");
            return (Criteria) this;
        }

        public Criteria andNamespaceNameLessThan(String value) {
            addCriterion("namespace_name <", value, "namespaceName");
            return (Criteria) this;
        }

        public Criteria andNamespaceNameLessThanOrEqualTo(String value) {
            addCriterion("namespace_name <=", value, "namespaceName");
            return (Criteria) this;
        }

        public Criteria andNamespaceNameLike(String value) {
            addCriterion("namespace_name like", value, "namespaceName");
            return (Criteria) this;
        }

        public Criteria andNamespaceNameNotLike(String value) {
            addCriterion("namespace_name not like", value, "namespaceName");
            return (Criteria) this;
        }

        public Criteria andNamespaceNameIn(List<String> values) {
            addCriterion("namespace_name in", values, "namespaceName");
            return (Criteria) this;
        }

        public Criteria andNamespaceNameNotIn(List<String> values) {
            addCriterion("namespace_name not in", values, "namespaceName");
            return (Criteria) this;
        }

        public Criteria andNamespaceNameBetween(String value1, String value2) {
            addCriterion("namespace_name between", value1, value2, "namespaceName");
            return (Criteria) this;
        }

        public Criteria andNamespaceNameNotBetween(String value1, String value2) {
            addCriterion("namespace_name not between", value1, value2, "namespaceName");
            return (Criteria) this;
        }

        public Criteria andAppIdIsNull() {
            addCriterion("app_id is null");
            return (Criteria) this;
        }

        public Criteria andAppIdIsNotNull() {
            addCriterion("app_id is not null");
            return (Criteria) this;
        }

        public Criteria andAppIdEqualTo(String value) {
            addCriterion("app_id =", value, "appId");
            return (Criteria) this;
        }

        public Criteria andAppIdNotEqualTo(String value) {
            addCriterion("app_id <>", value, "appId");
            return (Criteria) this;
        }

        public Criteria andAppIdGreaterThan(String value) {
            addCriterion("app_id >", value, "appId");
            return (Criteria) this;
        }

        public Criteria andAppIdGreaterThanOrEqualTo(String value) {
            addCriterion("app_id >=", value, "appId");
            return (Criteria) this;
        }

        public Criteria andAppIdLessThan(String value) {
            addCriterion("app_id <", value, "appId");
            return (Criteria) this;
        }

        public Criteria andAppIdLessThanOrEqualTo(String value) {
            addCriterion("app_id <=", value, "appId");
            return (Criteria) this;
        }

        public Criteria andAppIdLike(String value) {
            addCriterion("app_id like", value, "appId");
            return (Criteria) this;
        }

        public Criteria andAppIdNotLike(String value) {
            addCriterion("app_id not like", value, "appId");
            return (Criteria) this;
        }

        public Criteria andAppIdIn(List<String> values) {
            addCriterion("app_id in", values, "appId");
            return (Criteria) this;
        }

        public Criteria andAppIdNotIn(List<String> values) {
            addCriterion("app_id not in", values, "appId");
            return (Criteria) this;
        }

        public Criteria andAppIdBetween(String value1, String value2) {
            addCriterion("app_id between", value1, value2, "appId");
            return (Criteria) this;
        }

        public Criteria andAppIdNotBetween(String value1, String value2) {
            addCriterion("app_id not between", value1, value2, "appId");
            return (Criteria) this;
        }

        public Criteria andAppDescriptionIsNull() {
            addCriterion("app_description is null");
            return (Criteria) this;
        }

        public Criteria andAppDescriptionIsNotNull() {
            addCriterion("app_description is not null");
            return (Criteria) this;
        }

        public Criteria andAppDescriptionEqualTo(String value) {
            addCriterion("app_description =", value, "appDescription");
            return (Criteria) this;
        }

        public Criteria andAppDescriptionNotEqualTo(String value) {
            addCriterion("app_description <>", value, "appDescription");
            return (Criteria) this;
        }

        public Criteria andAppDescriptionGreaterThan(String value) {
            addCriterion("app_description >", value, "appDescription");
            return (Criteria) this;
        }

        public Criteria andAppDescriptionGreaterThanOrEqualTo(String value) {
            addCriterion("app_description >=", value, "appDescription");
            return (Criteria) this;
        }

        public Criteria andAppDescriptionLessThan(String value) {
            addCriterion("app_description <", value, "appDescription");
            return (Criteria) this;
        }

        public Criteria andAppDescriptionLessThanOrEqualTo(String value) {
            addCriterion("app_description <=", value, "appDescription");
            return (Criteria) this;
        }

        public Criteria andAppDescriptionLike(String value) {
            addCriterion("app_description like", value, "appDescription");
            return (Criteria) this;
        }

        public Criteria andAppDescriptionNotLike(String value) {
            addCriterion("app_description not like", value, "appDescription");
            return (Criteria) this;
        }

        public Criteria andAppDescriptionIn(List<String> values) {
            addCriterion("app_description in", values, "appDescription");
            return (Criteria) this;
        }

        public Criteria andAppDescriptionNotIn(List<String> values) {
            addCriterion("app_description not in", values, "appDescription");
            return (Criteria) this;
        }

        public Criteria andAppDescriptionBetween(String value1, String value2) {
            addCriterion("app_description between", value1, value2, "appDescription");
            return (Criteria) this;
        }

        public Criteria andAppDescriptionNotBetween(String value1, String value2) {
            addCriterion("app_description not between", value1, value2, "appDescription");
            return (Criteria) this;
        }

        public Criteria andIsDeletedIsNull() {
            addCriterion("is_deleted is null");
            return (Criteria) this;
        }

        public Criteria andIsDeletedIsNotNull() {
            addCriterion("is_deleted is not null");
            return (Criteria) this;
        }

        public Criteria andIsDeletedEqualTo(Boolean value) {
            addCriterion("is_deleted =", value, "isDeleted");
            return (Criteria) this;
        }

        public Criteria andIsDeletedNotEqualTo(Boolean value) {
            addCriterion("is_deleted <>", value, "isDeleted");
            return (Criteria) this;
        }

        public Criteria andIsDeletedGreaterThan(Boolean value) {
            addCriterion("is_deleted >", value, "isDeleted");
            return (Criteria) this;
        }

        public Criteria andIsDeletedGreaterThanOrEqualTo(Boolean value) {
            addCriterion("is_deleted >=", value, "isDeleted");
            return (Criteria) this;
        }

        public Criteria andIsDeletedLessThan(Boolean value) {
            addCriterion("is_deleted <", value, "isDeleted");
            return (Criteria) this;
        }

        public Criteria andIsDeletedLessThanOrEqualTo(Boolean value) {
            addCriterion("is_deleted <=", value, "isDeleted");
            return (Criteria) this;
        }

        public Criteria andIsDeletedIn(List<Boolean> values) {
            addCriterion("is_deleted in", values, "isDeleted");
            return (Criteria) this;
        }

        public Criteria andIsDeletedNotIn(List<Boolean> values) {
            addCriterion("is_deleted not in", values, "isDeleted");
            return (Criteria) this;
        }

        public Criteria andIsDeletedBetween(Boolean value1, Boolean value2) {
            addCriterion("is_deleted between", value1, value2, "isDeleted");
            return (Criteria) this;
        }

        public Criteria andIsDeletedNotBetween(Boolean value1, Boolean value2) {
            addCriterion("is_deleted not between", value1, value2, "isDeleted");
            return (Criteria) this;
        }

        public Criteria andDataChangeCreatedByIsNull() {
            addCriterion("data_change_created_by is null");
            return (Criteria) this;
        }

        public Criteria andDataChangeCreatedByIsNotNull() {
            addCriterion("data_change_created_by is not null");
            return (Criteria) this;
        }

        public Criteria andDataChangeCreatedByEqualTo(String value) {
            addCriterion("data_change_created_by =", value, "dataChangeCreatedBy");
            return (Criteria) this;
        }

        public Criteria andDataChangeCreatedByNotEqualTo(String value) {
            addCriterion("data_change_created_by <>", value, "dataChangeCreatedBy");
            return (Criteria) this;
        }

        public Criteria andDataChangeCreatedByGreaterThan(String value) {
            addCriterion("data_change_created_by >", value, "dataChangeCreatedBy");
            return (Criteria) this;
        }

        public Criteria andDataChangeCreatedByGreaterThanOrEqualTo(String value) {
            addCriterion("data_change_created_by >=", value, "dataChangeCreatedBy");
            return (Criteria) this;
        }

        public Criteria andDataChangeCreatedByLessThan(String value) {
            addCriterion("data_change_created_by <", value, "dataChangeCreatedBy");
            return (Criteria) this;
        }

        public Criteria andDataChangeCreatedByLessThanOrEqualTo(String value) {
            addCriterion("data_change_created_by <=", value, "dataChangeCreatedBy");
            return (Criteria) this;
        }

        public Criteria andDataChangeCreatedByLike(String value) {
            addCriterion("data_change_created_by like", value, "dataChangeCreatedBy");
            return (Criteria) this;
        }

        public Criteria andDataChangeCreatedByNotLike(String value) {
            addCriterion("data_change_created_by not like", value, "dataChangeCreatedBy");
            return (Criteria) this;
        }

        public Criteria andDataChangeCreatedByIn(List<String> values) {
            addCriterion("data_change_created_by in", values, "dataChangeCreatedBy");
            return (Criteria) this;
        }

        public Criteria andDataChangeCreatedByNotIn(List<String> values) {
            addCriterion("data_change_created_by not in", values, "dataChangeCreatedBy");
            return (Criteria) this;
        }

        public Criteria andDataChangeCreatedByBetween(String value1, String value2) {
            addCriterion("data_change_created_by between", value1, value2, "dataChangeCreatedBy");
            return (Criteria) this;
        }

        public Criteria andDataChangeCreatedByNotBetween(String value1, String value2) {
            addCriterion("data_change_created_by not between", value1, value2, "dataChangeCreatedBy");
            return (Criteria) this;
        }

        public Criteria andDataChangeCreatedTimeIsNull() {
            addCriterion("data_change_created_time is null");
            return (Criteria) this;
        }

        public Criteria andDataChangeCreatedTimeIsNotNull() {
            addCriterion("data_change_created_time is not null");
            return (Criteria) this;
        }

        public Criteria andDataChangeCreatedTimeEqualTo(LocalDateTime value) {
            addCriterion("data_change_created_time =", value, "dataChangeCreatedTime");
            return (Criteria) this;
        }

        public Criteria andDataChangeCreatedTimeNotEqualTo(LocalDateTime value) {
            addCriterion("data_change_created_time <>", value, "dataChangeCreatedTime");
            return (Criteria) this;
        }

        public Criteria andDataChangeCreatedTimeGreaterThan(LocalDateTime value) {
            addCriterion("data_change_created_time >", value, "dataChangeCreatedTime");
            return (Criteria) this;
        }

        public Criteria andDataChangeCreatedTimeGreaterThanOrEqualTo(LocalDateTime value) {
            addCriterion("data_change_created_time >=", value, "dataChangeCreatedTime");
            return (Criteria) this;
        }

        public Criteria andDataChangeCreatedTimeLessThan(LocalDateTime value) {
            addCriterion("data_change_created_time <", value, "dataChangeCreatedTime");
            return (Criteria) this;
        }

        public Criteria andDataChangeCreatedTimeLessThanOrEqualTo(LocalDateTime value) {
            addCriterion("data_change_created_time <=", value, "dataChangeCreatedTime");
            return (Criteria) this;
        }

        public Criteria andDataChangeCreatedTimeIn(List<LocalDateTime> values) {
            addCriterion("data_change_created_time in", values, "dataChangeCreatedTime");
            return (Criteria) this;
        }

        public Criteria andDataChangeCreatedTimeNotIn(List<LocalDateTime> values) {
            addCriterion("data_change_created_time not in", values, "dataChangeCreatedTime");
            return (Criteria) this;
        }

        public Criteria andDataChangeCreatedTimeBetween(LocalDateTime value1, LocalDateTime value2) {
            addCriterion("data_change_created_time between", value1, value2, "dataChangeCreatedTime");
            return (Criteria) this;
        }

        public Criteria andDataChangeCreatedTimeNotBetween(LocalDateTime value1, LocalDateTime value2) {
            addCriterion("data_change_created_time not between", value1, value2, "dataChangeCreatedTime");
            return (Criteria) this;
        }

        public Criteria andDataChangeLastModifiedByIsNull() {
            addCriterion("data_change_last_modified_by is null");
            return (Criteria) this;
        }

        public Criteria andDataChangeLastModifiedByIsNotNull() {
            addCriterion("data_change_last_modified_by is not null");
            return (Criteria) this;
        }

        public Criteria andDataChangeLastModifiedByEqualTo(String value) {
            addCriterion("data_change_last_modified_by =", value, "dataChangeLastModifiedBy");
            return (Criteria) this;
        }

        public Criteria andDataChangeLastModifiedByNotEqualTo(String value) {
            addCriterion("data_change_last_modified_by <>", value, "dataChangeLastModifiedBy");
            return (Criteria) this;
        }

        public Criteria andDataChangeLastModifiedByGreaterThan(String value) {
            addCriterion("data_change_last_modified_by >", value, "dataChangeLastModifiedBy");
            return (Criteria) this;
        }

        public Criteria andDataChangeLastModifiedByGreaterThanOrEqualTo(String value) {
            addCriterion("data_change_last_modified_by >=", value, "dataChangeLastModifiedBy");
            return (Criteria) this;
        }

        public Criteria andDataChangeLastModifiedByLessThan(String value) {
            addCriterion("data_change_last_modified_by <", value, "dataChangeLastModifiedBy");
            return (Criteria) this;
        }

        public Criteria andDataChangeLastModifiedByLessThanOrEqualTo(String value) {
            addCriterion("data_change_last_modified_by <=", value, "dataChangeLastModifiedBy");
            return (Criteria) this;
        }

        public Criteria andDataChangeLastModifiedByLike(String value) {
            addCriterion("data_change_last_modified_by like", value, "dataChangeLastModifiedBy");
            return (Criteria) this;
        }

        public Criteria andDataChangeLastModifiedByNotLike(String value) {
            addCriterion("data_change_last_modified_by not like", value, "dataChangeLastModifiedBy");
            return (Criteria) this;
        }

        public Criteria andDataChangeLastModifiedByIn(List<String> values) {
            addCriterion("data_change_last_modified_by in", values, "dataChangeLastModifiedBy");
            return (Criteria) this;
        }

        public Criteria andDataChangeLastModifiedByNotIn(List<String> values) {
            addCriterion("data_change_last_modified_by not in", values, "dataChangeLastModifiedBy");
            return (Criteria) this;
        }

        public Criteria andDataChangeLastModifiedByBetween(String value1, String value2) {
            addCriterion("data_change_last_modified_by between", value1, value2, "dataChangeLastModifiedBy");
            return (Criteria) this;
        }

        public Criteria andDataChangeLastModifiedByNotBetween(String value1, String value2) {
            addCriterion("data_change_last_modified_by not between", value1, value2, "dataChangeLastModifiedBy");
            return (Criteria) this;
        }

        public Criteria andDataChangeLastModifiedTimeIsNull() {
            addCriterion("data_change_last_modified_time is null");
            return (Criteria) this;
        }

        public Criteria andDataChangeLastModifiedTimeIsNotNull() {
            addCriterion("data_change_last_modified_time is not null");
            return (Criteria) this;
        }

        public Criteria andDataChangeLastModifiedTimeEqualTo(LocalDateTime value) {
            addCriterion("data_change_last_modified_time =", value, "dataChangeLastModifiedTime");
            return (Criteria) this;
        }

        public Criteria andDataChangeLastModifiedTimeNotEqualTo(LocalDateTime value) {
            addCriterion("data_change_last_modified_time <>", value, "dataChangeLastModifiedTime");
            return (Criteria) this;
        }

        public Criteria andDataChangeLastModifiedTimeGreaterThan(LocalDateTime value) {
            addCriterion("data_change_last_modified_time >", value, "dataChangeLastModifiedTime");
            return (Criteria) this;
        }

        public Criteria andDataChangeLastModifiedTimeGreaterThanOrEqualTo(LocalDateTime value) {
            addCriterion("data_change_last_modified_time >=", value, "dataChangeLastModifiedTime");
            return (Criteria) this;
        }

        public Criteria andDataChangeLastModifiedTimeLessThan(LocalDateTime value) {
            addCriterion("data_change_last_modified_time <", value, "dataChangeLastModifiedTime");
            return (Criteria) this;
        }

        public Criteria andDataChangeLastModifiedTimeLessThanOrEqualTo(LocalDateTime value) {
            addCriterion("data_change_last_modified_time <=", value, "dataChangeLastModifiedTime");
            return (Criteria) this;
        }

        public Criteria andDataChangeLastModifiedTimeIn(List<LocalDateTime> values) {
            addCriterion("data_change_last_modified_time in", values, "dataChangeLastModifiedTime");
            return (Criteria) this;
        }

        public Criteria andDataChangeLastModifiedTimeNotIn(List<LocalDateTime> values) {
            addCriterion("data_change_last_modified_time not in", values, "dataChangeLastModifiedTime");
            return (Criteria) this;
        }

        public Criteria andDataChangeLastModifiedTimeBetween(LocalDateTime value1, LocalDateTime value2) {
            addCriterion("data_change_last_modified_time between", value1, value2, "dataChangeLastModifiedTime");
            return (Criteria) this;
        }

        public Criteria andDataChangeLastModifiedTimeNotBetween(LocalDateTime value1, LocalDateTime value2) {
            addCriterion("data_change_last_modified_time not between", value1, value2, "dataChangeLastModifiedTime");
            return (Criteria) this;
        }
    }

    /**
     */
    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}